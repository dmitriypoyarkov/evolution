using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{
    [SerializeField] private float maxSpeed = 1f;
    public Rigidbody2D rb;

    public float minForce = 3f;
    public float forceScaling = 9f;
    
    public Vector2 SmoothSpeed
    {
        get
        {
            if (velocityBuf.Count == 0)
            {
                return Vector2.zero;
            }
            return velocityBuf.Aggregate((acc, cur) => acc + cur) / velocityBuf.Count;
        }
    }
    public float MovementForce { get; set; }
    private int bufCapacity = 6;
    private List<Vector2> velocityBuf;


    // Start is called before the first frame update
    void Start()
    {
        velocityBuf = new List<Vector2>(bufCapacity);
    }

    public void Move(Vector2 direction)
    {
        if (direction.magnitude > 0)
        {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        if (direction.magnitude > 1)
        {
            direction.Normalize();
        }
        rb.AddForce(MovementForce * direction);
    }

    public void FixedUpdate()
    {
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
        AddToVelocityBuf(rb.velocity);
    }

    private void AddToVelocityBuf(Vector2 velocity)
    {
        if (velocityBuf.Count == 15)
        {
            velocityBuf.RemoveAt(0);
        }
        velocityBuf.Add(velocity);
    }
}
