using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityRecord
{
    public int[] dimensions;
    public float[] networkContent;

    public float movementForcePoints;
    public float sharpnessPoints;
    public float maxHealthPoints;
    public bool rotten;
    public float lifetime;
    public int kills;
    public int foods;

    public EntityRecord(EntityRecord record)
    {
        dimensions = record.dimensions;
        networkContent = record.networkContent;
        lifetime = record.lifetime;
        kills = record.kills;
        foods = record.foods;
        movementForcePoints = record.movementForcePoints;
        sharpnessPoints = record.sharpnessPoints;
        maxHealthPoints = record.maxHealthPoints;
    }

    public EntityRecord(Entity entity)
    {
        dimensions = entity.network.dimensions.ToArray();
        networkContent = entity.network.networkContent.ToArray();
        lifetime = entity.Lifetime;
        kills = entity.Kills;
        foods = entity.Foods;
        movementForcePoints = entity.MovementForcePoints;
        sharpnessPoints = entity.SharpnessPoints;
        maxHealthPoints = entity.MaxHealthPoints;
        rotten = entity.rotten;
    }
}
