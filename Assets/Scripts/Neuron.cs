using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Neuron
{
    public float bias;
    public float value;
    public float[] connectionWeights;
}
