using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static System.Linq.Enumerable;

public class FoodSpawner : MonoBehaviour
{
    [SerializeField] private GameObject foodGo;
    [SerializeField] private float spawnPeriod = 2f;
    [SerializeField] private int startFood = 5;
    [SerializeField] private int maxFood = 8;
    [SerializeField] private SpriteRenderer field;
    private bool isSpawning = true;

    public static List<Food> foods = new List<Food>();
    // Start is called before the first frame update
    private void Start()
    {
        SpawnMultipleFood(startFood);
    }

    public void Stop()
    {
        isSpawning = false;
    }

    public void Resume()
    {
        isSpawning = true;
    }

    private void SpawnMultipleFood(int amount)
    {
        foreach (var _ in Range(0, amount))
        {
            SpawnFood();
        }
    }

    private void SpawnFood()
    {
        if (foods.Count() > maxFood || !isSpawning)
        {
            return;
        }
        var offset = new Vector3(-field.bounds.size.x * 0.5f, -field.bounds.size.x * 0.5f, 0);
        var position = new Vector3(field.bounds.size.x * Random.Range(0f, 1f),
                field.bounds.size.y * Random.Range(0f, 1f), 0f);
        foods.Add(Instantiate(foodGo, offset + position, Quaternion.identity)
                    .GetComponent<Food>());
    }

    float lastSpawnTime = 0f;
    private void FixedUpdate()
    {
        if (Time.time - lastSpawnTime < spawnPeriod)
        {
            SpawnFood();
            lastSpawnTime = Time.time;
        }
    }
}
