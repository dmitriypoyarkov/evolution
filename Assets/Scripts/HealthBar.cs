using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Offensive offensive;
    [SerializeField] private Slider slider;

    private void Start()
    {
        
    }

    private void Update()
    {
        slider.maxValue = offensive.MaxHealth;
        slider.value = offensive.Health;
    }
}
