using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static System.Linq.Enumerable;
using Unity.Jobs;
using System.IO;

public class EntityController : MonoBehaviour
{
    [SerializeField] SpriteRenderer field;
    [SerializeField] RecordHolder recordHolder;
    [SerializeField] GameObject entityGo;
    [SerializeField] string loadFromFile;

    [SerializeField] private List<int> dimensions;
    [SerializeField] private int entityVerticalNumber = 10;
    [SerializeField] private int entityHorizontalNumber = 10;
    [SerializeField] private float eyefieldRadius = 10f;
    [SerializeField] private float networkStep = 0.1f;
    [Range(1, 50)]
    [SerializeField] private float timeScale;
    [SerializeField] private FoodSpawner foodSpawner;

    PhysicsScene2D scene;
    private float radiusStep;
    private float[] output;
    private ContactFilter2D contactFilter;
    private float[,] inputs;
    private IEnumerable<JobHandle> tasks;

    private float time;
    private Vector2 gridSize;

    public delegate void FinishGenerationEvent();
    static FinishGenerationEvent finishGenerationEvent;

    // Start is called before the first frame update
    void Start()
    {
        time = 0f;
        radiusStep = 0.25f * eyefieldRadius;
        contactFilter = new ContactFilter2D();
        contactFilter.NoFilter();
        gridSize = new Vector2(field.bounds.size.x / (entityHorizontalNumber + 1),
            field.bounds.size.y / (entityVerticalNumber + 1));

        SpawnEntities();
        ConfigureEntities();

        finishGenerationEvent += FinishSimulation;

        StartCoroutine(BeginFinishCountdown());

        //InitializeNetworks();
        //Physics.autoSimulation = false;

        //Physics2D.simulationMode = SimulationMode2D.Script;
        //scene = SceneManager.GetActiveScene().GetPhysicsScene2D();
        //Invoke("Simulate", 0.1f);
    }

    IEnumerator BeginFinishCountdown()
    {
        yield return new WaitForSeconds(100f);
        finishGenerationEvent?.Invoke();
    }

    void SpawnEntities()
    {
        foreach (int y in Range(1, entityVerticalNumber))
        {
            foreach (int x in Range(1, entityHorizontalNumber))
            {
                var position = new Vector2(x * gridSize.x + field.bounds.min.x,
                    y * gridSize.y + field.bounds.min.y);
                var entity = Instantiate(entityGo, position, Quaternion.identity, transform);
                entity.GetComponent<Entity>().recordHolder = recordHolder;
            }
        }
    }

    void ConfigureEntities()
    {
        if (RecordHolder.archive.Count == 0) //then initialize
        {
            Debug.Log("Initializing");
            if (loadFromFile.Length > 0) // from file or
            {
                var fileContent = File.ReadAllText(loadFromFile);
                var records = JsonHelper.FromJson<EntityRecord>(fileContent);
                for (int i = 0; i < records.Length; i++)
                {
                    Entity.entities[i].network = new NeuralNetwork(records[i]);
                    Entity.entities[i].MaxHealthPoints = records[i].maxHealthPoints;
                    Entity.entities[i].MovementForcePoints = records[i].movementForcePoints;
                    Entity.entities[i].SharpnessPoints = records[i].sharpnessPoints;
                }
            }
            else // or without file
            {
                float norm;
                for (int i = 0; i < Entity.entities.Count; i++)
                {
                    Entity.entities[i].network = new NeuralNetwork(dimensions);
                    var randomPoints = new List<float>(
                        Range(0, 3)
                        .Select(_ => Random.Range(0.01f, 1f))
                        .ToList()
                    );
                    norm = randomPoints.Sum();
                    randomPoints = randomPoints.Select(x => x / norm).ToList();
                    Entity.entities[i].MaxHealthPoints = randomPoints[0];
                    Entity.entities[i].MovementForcePoints = randomPoints[1];
                    Entity.entities[i].SharpnessPoints = randomPoints[2];
                }
            }
            return;
        }
        Debug.Log("Next Gen");
        //take previous generation, select and mutate it
        var previousGen = RecordHolder.archive.Last();
        var rng = new System.Random();
        rng.Shuffle(previousGen);
        EntityRecord winner;
        for (int i = 0; i < Entity.entities.Count / 1; i += 2)
        {
            if (previousGen[i].rotten && !previousGen[i+1].rotten)
            {
                winner = previousGen[i + 1];
            }
            else if (!previousGen[i].rotten && previousGen[i + 1].rotten)
            {
                winner = previousGen[i];
            }
            if (previousGen[i].foods > previousGen[i + 1].foods)
            {
                winner = previousGen[i];
            }
            else 
            {
                winner = previousGen[i + 1];
            }
            
            Entity.entities[i].network = new NeuralNetwork(winner);
            Entity.entities[i].MaxHealthPoints = winner.maxHealthPoints;
            Entity.entities[i].MovementForcePoints = winner.movementForcePoints;
            Entity.entities[i].SharpnessPoints = winner.sharpnessPoints;

            var mutatedNetwork = new EntityRecord(winner);
            float mutation;
            for (int j = 0; j < mutatedNetwork.networkContent.Length; j++)
            {
                mutation = networkStep * Gaussian();
                mutatedNetwork.networkContent[j] = Mathf.Clamp(mutatedNetwork.networkContent[j] - mutation, -1f,
                    1f);
            }
            var mutatedPoints = new float[] {
                Mathf.Clamp01(mutatedNetwork.maxHealthPoints + 0.3f * Gaussian()),
                Mathf.Clamp01(mutatedNetwork.movementForcePoints + 0.1f * Gaussian()),
                Mathf.Clamp01(mutatedNetwork.sharpnessPoints + 0.03f * Gaussian()),
            };
            var norm = mutatedPoints.Sum();
            for (int j = 0; j < mutatedPoints.Length; j++)
            {
                mutatedPoints[j] /= norm;
            }

            Entity.entities[i + 1].network = new NeuralNetwork(mutatedNetwork);
            Entity.entities[i + 1].MaxHealthPoints = mutatedPoints[0];
            Entity.entities[i + 1].MovementForcePoints = mutatedPoints[1];
            Entity.entities[i + 1].SharpnessPoints = mutatedPoints[2];
        }
    }

    public static float Gaussian()
    {
        float sigma = 1f;
        float x = Random.Range(-4f, 4f);

        int randomSign = Random.Range(0, 2) == 0 ? -1 : 1;
        return randomSign / (sigma * Mathf.Sqrt(2 * Mathf.PI)) *
            Mathf.Exp(-0.5f * Mathf.Pow(x / sigma, 2));
    }

    void FinishSimulation()
    {
        Entity.entities.ForEach(e => Destroy(e.gameObject));

        
        StartCoroutine(DestroyFood());
    }

    IEnumerator DestroyFood()
    {
        foodSpawner.Stop();
        while (true)
        {
            var foods = FindObjectsOfType<Food>().ToList();
            if (foods.Count == 0)
            {
                break;
            }
            foods.ForEach(f => Destroy(f.gameObject));
            yield return new WaitForSeconds(0f);
        }
        foodSpawner.Resume();
        StartCoroutine(RestartSimulation());
    }

    IEnumerator RestartSimulation()
    {
        while (Entity.entities.Count > 0)
        {
            yield return new WaitForFixedUpdate();
        }
        recordHolder.MoveRecordsToArchive();
        SpawnEntities();
        ConfigureEntities();
        StartCoroutine(BeginFinishCountdown());
    }

    void Simulate()
    {
        foreach (int i in Range(0, 5000))
        {
            FixedUpdate();
            //EntitiesStep();
            scene.Simulate(Time.fixedDeltaTime);
            time += Time.fixedDeltaTime;
        }
    }

    void InitializeNetworks()
    {
        //Entity.entities.ForEach(e => e.network.Initialize(dimensions));
    }

    public static T NearestObject<T>(Vector3 center, List<T> objects) where T : MonoBehaviour
    {
        if (objects.Count == 0)
        {
            return null;
        }
        var nearest = objects[0];
        var minDistance = (center - nearest.transform.position).magnitude;
        foreach(var obj in objects)
        {
            var curDistance = (obj.transform.position - center).magnitude;
            if (curDistance < minDistance)
            {
                nearest = obj;
                minDistance = curDistance;
            }
        }
        return nearest;
    }

    public static float NormalizeHealth(float health)
    {
        return health / Entity.maxPossibleHealth;
    }

    JobHandle EntityStep(Entity entity)
    {
        if (entity.transform.position.magnitude > field.bounds.size.magnitude * 0.55)
        {
            entity.Rot();
        }
        var nearestFood = entity.NearestFood(Time.time);
        var nearestEnemy = entity.NearestEnemy(Time.time);

        var enemyRadius = nearestEnemy ? (Vector2)(nearestEnemy.transform.position - entity.transform.position) :
            new Vector2(Mathf.Infinity, Mathf.Infinity);
        var foodRadius = nearestFood ? (Vector2)(nearestFood.transform.position - entity.transform.position) :
            new Vector2(Mathf.Infinity, Mathf.Infinity);

        var selfSpeed = entity.movement.rb.velocity;
        var enemySpeed = nearestEnemy ? nearestEnemy.movement.rb.velocity : Vector2.zero;

        var inputArray = new float[] {
                NeuralNetwork.Sigmoid(enemyRadius.x),
                NeuralNetwork.Sigmoid(enemyRadius.y),
                NeuralNetwork.Sigmoid(foodRadius.x),
                NeuralNetwork.Sigmoid(foodRadius.y),
                NormalizeHealth(entity.offensive.Health),
                nearestEnemy ? NormalizeHealth(nearestEnemy.GetComponent<Offensive>().Health)
                             : 0f,
                NeuralNetwork.Sigmoid(selfSpeed.x),
                NeuralNetwork.Sigmoid(selfSpeed.y),
                NeuralNetwork.Sigmoid(enemySpeed.x),
                NeuralNetwork.Sigmoid(enemySpeed.y),
            };
        entity.network.SetInput(inputArray);
        return entity.network.Schedule();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Time.timeScale = timeScale;
        inputs = new float[Entity.entities.Count, 6];
        tasks = Entity.entities.Select(e => EntityStep(e) );
        
        int i = 0;
        foreach (var task in tasks)
        {
            task.Complete();
            output = Entity.entities[i].network.output.ToArray();
            Entity.entities[i].movement.Move(new Vector2(output[0], output[1]));
            i++;
        }
    }
}