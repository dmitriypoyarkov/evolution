using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class RecordHolder : MonoBehaviour
{
    [SerializeField] string archivePath;
    [SerializeField] string filenameBase;

    private string dirPath;
    public static List<List<EntityRecord>> archive = new List<List<EntityRecord>>();

    private static List<EntityRecord> currentRecords = new List<EntityRecord>();
    private int archiveCount = 0;
    // Start is called before the first frame update
    private void OnEnable()
    {
        dirPath = archivePath + System.DateTime.Now.ToString();
        dirPath = dirPath.Replace(":", "-");
        Directory.CreateDirectory(dirPath);
    }

    public void AddRecord(EntityRecord record)
    {
        currentRecords.Add(record);
        var str = JsonUtility.ToJson(record);
    }

    public void MoveRecordsToArchive()
    {
        if (archive.Count == 10)
        {
            var fullPath = dirPath + "/" + filenameBase + archiveCount.ToString() + ".json";
            using (StreamWriter sw = File.CreateText(fullPath))
            {
                var str = JsonHelper.ToJson(archive[archive.Count - 1].ToArray(), true);
                sw.Write(str);
            }
            archive.Clear();
            archiveCount += 10;
        }
        archive.Add(new List<EntityRecord>(currentRecords));
        currentRecords.Clear();
    }
}
