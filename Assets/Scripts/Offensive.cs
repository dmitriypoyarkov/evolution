using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Offensive : MonoBehaviour
{
    public float Sharpness { get; set; }
    private Hit GetHit {
        get => new Hit(
                Sharpness,
                movement.SmoothSpeed,
                transform.position
            );
    }
    [SerializeField] private float speedDamageScaling = 1f;

    [SerializeField] private Movement movement;
    [SerializeField] private Collider2D col;
    [SerializeField] private Entity entity;

    [SerializeField] public float minSharpness = 0.5f;
    [SerializeField] public float sharpnessScaling = 4f;

    [SerializeField] public float minMaxHealth = 6f;
    [SerializeField] public float healthScaling = 30f;

    private float maxHealth;
    public float MaxHealth {
        get => maxHealth;
        set
        {
            maxHealth = value;
            Health = value;
        }
    }
    private float health;
    public float Health {
        get => health;
        set
        {
            health = Mathf.Clamp(value, 0, MaxHealth);
        }
    }

    private void Start()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Finish"))
        {
            entity.Rot();
        }
        var otherOffensive = collision.gameObject.GetComponent<Offensive>();
        if (otherOffensive is null)
        {
            return;
        }
        otherOffensive.TakeDamage(GetHit);
    }

    public void TakeDamage(Hit hit)
    {
        var radiusVector = (Vector2)transform.position - hit.source;
        float damageFactor = Mathf.Clamp(Vector2.Dot(speedDamageScaling * hit.velocity, radiusVector.normalized), 0f, Mathf.Infinity);
        Health -= hit.damage * (1 + damageFactor);
    }

    public void FixedUpdate()
    {
        if (Health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void RestoreHealth(float amount)
    {
        Health += amount;
        entity.Foods += 1;
    }
}
