using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Entity : MonoBehaviour
{

    /*
    * �� ���� ����������� ��������� ���;
    * �� ������� ������ ���� ������ ���, ������������� ����������,
    * ��� ��������������� ��������;
    * ��������� �����, �������� ������ ������;
    ���� ����� ������� �� ��������, � ������� ����������
    � ���� ���������� ����� �����;
    ���� ����� ������������ ��:
    - ������������ ���������
    - ���������� ������
    - ������� �������
    ����������� ���������:
    ���� (��� ���������� ��������� � [0, 1] ��������������� r/(1+r):
    - ���������� �� ���������� ����� �� x
    - ���������� �� ���������� ����� �� y
    - ���������� �� ��������� ��� �� x
    - ���������� �� ��������� ��� �� y

    - ���������� ����� ������
    - ���������� ������ ���������� �����
    �����:
    - ��������� �� x
    - ��������� �� y

    ��������� ���������:
    - ������� �� ����������� ������, �������� ������ ���
    - ���������� ��������� ����������� ������
     
    ��������� ����:
    - ��� � ������� ��������� � ������ - ������� �� ������, �������� �������� ������ ���
    - ��� � ������� ��������� � ������ - ���������� �������� ������
    - ��� � ������� ������ � ������ - ��������, �� ��� ���� ���������� ��������� ������������,
    �������������� ����� � ����� (���� �������� �� ����� ������ ������� ������� ������ ��� ����)

    ��-������, ����� ���������� ����� ������ ��������� ������ ���� ���-�� ������,
    � ����� ��� ����� ����������� ���������.
    ��-������, ������� ����������� ����������� ����� ���������, ������ � ��������.
    ��� ������ ����� ����������� �����������, ��� ������� ��������� ������������ �����.
    

    ��������� �����������:
    - ����������������� - �������� ���������� ������ �������. ������ ������ = ������.
    ����� ����������� ���� ������, ����� ������������ �� ��� ����. �������� ��� ��������������� 
    �� ������ �����, �� � ������� - ������������� ����������.
     */
    [SerializeField] private GameObject foodGo;

    [SerializeField] private float movementForce = 1f;
    [SerializeField] private float sharpness = 1f;
    [SerializeField] private float maxHealth = 1f;

    [SerializeField] public static float maxPossibleHealth = 50f;

    [SerializeField] public Movement movement;
    [SerializeField] public Offensive offensive;
    [SerializeField] public RecordHolder recordHolder;

    [SerializeField] private float targetRefreshTime;
    [SerializeField] private float eyefieldRadius = 30f;
    [SerializeField] private int foodSpawnAmount = 2;
    private float radiusStep;
    private ContactFilter2D contactFilter;
    private Entity targetEntity;
    private Food targetFood;
    private float randomTime;
    private float lastFoodSelectTime;
    public bool rotten = false;

    public NeuralNetwork network;

    public static List<Entity> entities = new List<Entity>();
    private float lastEnemySelectTime;
    private float creationTime;

    public int Kills { get; private set; }
    public int Foods { get; set; }

    private float movementForcePoints;
    public float MovementForcePoints
    {
        set
        {
            movementForcePoints = value;
            movement.MovementForce = movement.minForce + value * movement.forceScaling;
        }
        get => movementForcePoints;
    }
    private float sharpnessPoints;
    public float SharpnessPoints
    {
        set
        {
            sharpnessPoints = value;
            offensive.MaxHealth = offensive.minMaxHealth + value * offensive.healthScaling;
        }
        get => sharpnessPoints;
    }
    private float maxHealthPoints;

    public float MaxHealthPoints
    {
        set
        {
            maxHealthPoints = value;
            offensive.Sharpness = offensive.minSharpness + value * offensive.sharpnessScaling;
        }
        get => maxHealthPoints;
    }

    public float Lifetime
    {
        get => Time.time - creationTime;
    }

    public void Rot()
    {
        rotten = true;
    }

    private IEnumerator Die()
    {
        yield return new WaitForFixedUpdate();
        Destroy(gameObject);
    }

    void OnEnable()
    {
        lastFoodSelectTime = Random.Range(0f, 0.3f);
        entities.Add(this);
        radiusStep = 0.25f * eyefieldRadius;
        contactFilter = new ContactFilter2D();
        creationTime = Time.time;
    }

    private void OnDestroy()
    {
        entities.Remove(this);
        recordHolder.AddRecord(new EntityRecord(this));
        network.Destroy();
        if (!gameObject.scene.isLoaded)
        {
            return;
        }
        if (rotten)
        {
            return;
        }
        for (int i = 0; i < foodSpawnAmount; i++)
        {
            Instantiate(foodGo, transform.position, Quaternion.identity);
        }
    }

    public Entity NearestEnemy(float time)
    {
        if (time - lastEnemySelectTime < targetRefreshTime)
        {
            return this.targetEntity;
        }
        lastEnemySelectTime = time;
        var results = new List<Collider2D>();
        List<Entity> nearEntities;
        Entity nearestEnemy = null;
        for (float radius = radiusStep; radius <= eyefieldRadius; radius += radiusStep)
        {
            Physics2D.OverlapCircle(transform.position, radius, contactFilter, results);
            nearEntities = results.Select(r => r.GetComponent<Entity>())
                .Where(x => !(x is null) && !(x == this))
                .ToList();
            if (nearEntities.Count() > 0)
            {
                nearestEnemy = EntityController.NearestObject(transform.position, nearEntities);
                break;
            }
        }
        this.targetEntity = nearestEnemy;
        return nearestEnemy;
    }

    public Food NearestFood(float time)
    {
        if (time - lastFoodSelectTime < targetRefreshTime)
        {
            return this.targetFood;
        }
        lastFoodSelectTime = time;
        var results = new List<Collider2D>();
        List<Food> nearFoods;
        Food nearestFood = null;
        for (float radius = radiusStep; radius <= eyefieldRadius; radius += radiusStep)
        {
            Physics2D.OverlapCircle(transform.position, radius, contactFilter, results);
            nearFoods = results.Select(r => r.GetComponent<Food>())
                .Where(x => !(x is null))
                .ToList();
            if (nearFoods.Count() > 0)
            {
                nearestFood = EntityController.NearestObject(transform.position, nearFoods);
                break;
            }
        }
        this.targetFood = nearestFood;
        return nearestFood;
    }
}
