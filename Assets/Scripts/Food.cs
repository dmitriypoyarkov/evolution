using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Food : MonoBehaviour
{
    [SerializeField] float healthRestoreValue = 1f;

    void BeEaten()
    {
        Destroy(gameObject);
    }
   
    private void OnCollisionEnter2D(Collision2D coll)
    { 
        var offensive = coll.collider.GetComponent<Offensive>();
        if (offensive is null)
        {
            return;
        }
        BeEaten();
        offensive.RestoreHealth(healthRestoreValue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        FoodSpawner.foods.Remove(this);
    }
}
