using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit
{
    public float damage;
    public Vector2 velocity;
    public Vector2 source;

    public Hit(float damage, Vector2 velocity, Vector2 source)
    {
        this.damage = damage;
        this.velocity = velocity;
        this.source = source;
    }
}
