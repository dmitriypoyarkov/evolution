using UnityEngine;
using Unity.Collections;
using System.Collections.Generic;
using Unity.Jobs;

public struct NeuralNetwork : IJob
{
    public NativeArray<int> dimensions;
    public NativeArray<float> networkContent;
    public NativeArray<float> input;
    public NativeArray<float> output;
    public static float biasScale = 2f;

    public NeuralNetwork(List<int> dimensions)
    {
        this.dimensions = new NativeArray<int>(dimensions.Count, Allocator.Persistent);
        int contentAmount = 0;
        for (int i = 1; i < dimensions.Count; i++)
        {
            contentAmount += dimensions[i] * (2 + dimensions[i - 1]);
        }
        this.networkContent = new NativeArray<float>(contentAmount, Allocator.Persistent);
        this.input = new NativeArray<float>(dimensions[0], Allocator.Persistent);
        this.output = new NativeArray<float>(dimensions[dimensions.Count - 1], Allocator.Persistent);
        Initialize(dimensions);
    }

    public NeuralNetwork(EntityRecord record)
    {
        this.dimensions = new NativeArray<int>(record.dimensions, Allocator.Persistent);
        this.networkContent = new NativeArray<float>(record.networkContent, Allocator.Persistent);
        this.input = new NativeArray<float>(dimensions[0], Allocator.Persistent);
        this.output = new NativeArray<float>(dimensions[dimensions.Length - 1], Allocator.Persistent);
    }

    public void SetInput(float[] input)
    {
        //this.input = new NativeArray<float>(input.Length, Allocator.TempJob);
        //this.output = new NativeArray<float>(dimensions[dimensions.Length - 1], Allocator.TempJob);
        for (int i = 0; i < dimensions[0]; i++)
        {
            this.input[i] = input[i];
        }
    }

    public void Destroy()
    {
        if (networkContent.Length > 0) networkContent.Dispose();
        if (dimensions.Length > 0) dimensions.Dispose();
        if (input.Length > 0) input.Dispose();
        if (output.Length > 0) output.Dispose();
    }

    public void Initialize(List<int> dims)
    {
        for (int i = 0; i < dims.Count; i++)
        {
            dimensions[i] = dims[i];
        }
        
        int contentCount = 0;
        for (int k = 1; k < dimensions.Length; k++)
        {
            for (int i = 0; i < dimensions[k]; i++)
            {
                networkContent[contentCount++] = 0f; //value
                networkContent[contentCount++] = Random.Range(-1f, 1f); //bias
                for (int j = 0; j < dimensions[k-1]; j++)
                {
                    networkContent[contentCount++] = Random.Range(-1f, 1f);
                }
            }
        }
    }

    public static float Sigmoid(float x)
    {
        return 2f / (1f + Mathf.Exp(-x * 1f)) - 1f;
    }

    public void ProcessInput()
    {
        float value;
        int valueIndex;
        for (int i = 0; i < dimensions[1]; i++)
        {
            valueIndex = i * (dimensions[0] + 2);
            value = 0;
            for (int j = 0; j < dimensions[0]; j++)
            {
                value += input[j] * networkContent[valueIndex + 2 + j];
            }
            value += networkContent[valueIndex + 1] * biasScale;
            networkContent[valueIndex] = Sigmoid(value);
        }

        int prevLayerIndex;
        int startIndex;
        for (int k = 2; k < dimensions.Length; k++)
        {
            startIndex = 0;
            for (int i = 1; i < k; i++)
            {
                startIndex += dimensions[i] * (dimensions[i - 1] + 2);
            }
            prevLayerIndex = 0;
            for (int i = 1; i < k - 1; i++)
            {
                prevLayerIndex += dimensions[i] * (dimensions[i - 1] + 2);
            }
            for (int i = 0; i < dimensions[k]; i++)
            {
                valueIndex = startIndex + i * (dimensions[k - 1] + 2);
                value = 0;
                for (int j = 0; j < dimensions[k - 1]; j++)
                {
                    value += networkContent[prevLayerIndex + j * (dimensions[k - 2] + 2)] *
                        networkContent[valueIndex + 2 + j];
                }
                value += networkContent[valueIndex + 1] * biasScale; //bias
                networkContent[valueIndex] = Sigmoid(value);
            }
        }

        var lastIndex = 0;
        for (int i = 1; i < dimensions.Length - 1; i++)
        {
            lastIndex += dimensions[i] * (dimensions[i - 1] + 2);
        }
        for (int i = 0; i < dimensions[dimensions.Length - 1]; i++)
        {
            output[i] = networkContent[lastIndex + i * (dimensions[dimensions.Length - 2] + 2)];
        }


        //Range(0, input.Length)
        //    .Select(i => layers[0][i].value = input[i]);

        //foreach (int i in Range(1, layers.Count() - 1))
        //{
        //    Range(0, layers[i].Length)
        //        .Select(j =>
        //        layers[i][j].value = Sigmoid(
        //                Range(0, layers[i][j].connectionWeights.Length)
        //                .Select(k => layers[i][j].connectionWeights[k] * layers[i - 1][k].value)
        //                .Sum()
        //            )
        //        );
        //}
        //var output = layers.Last().Select(x => x.value).ToArray();
        ////Debug.Log($"Controls: {output[0]} {output[1]}");
        //return output;
    }

    public void Execute()
    {
        ProcessInput();
    }
}
